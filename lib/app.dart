import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:udder/home_page.dart';
import 'package:udder/scoped_model/main.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final MainModel _model = MainModel();
  @override
  void initState() {
    _model.preparemarker();
    _model.setavailableCars();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModel(
      child: MaterialApp(
          title: 'Flutter Demo',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: HomePage()),
      model: _model,
    );
  }
}
