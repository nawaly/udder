import 'package:flutter/material.dart';
import 'package:udder/map.dart';

class CurrentLocation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 0.85,
            child: MapPage(),
          ),
          Image.asset('assets/images/pickup_location.png'),
          Text('Current Location',
              style: TextStyle(
                color: Colors.white,
              ))
        ],
      ),
    );
  }
}
