import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:udder/Model/car.dart';
// model to share data to all app
mixin UdderConnectedModel on Model {
  List<Car> _availableCars = [];
  double _choiceWidgetHeight = 50;
  int _currentIndex = 0;
}
mixin UtilityModel on UdderConnectedModel {
//getters
  double get choiceWidgetHeight => _choiceWidgetHeight;
  int get currentIndex => _currentIndex;

//setters
  void setChoiceWidgetHeight({@required height}) {
    _choiceWidgetHeight = height;
    notifyListeners();
  }

  void setCurrentIndex({@required index}) {
    _currentIndex = index;
    notifyListeners();
  }
}
mixin CarsModel on UdderConnectedModel {
  List<Car> get availableCars => _availableCars;
  void setavailableCars() {
    _availableCars = cars;
    notifyListeners();
  }
}
mixin GoogleMapModel on UdderConnectedModel {
  Map<MarkerId, Marker> _markers = <MarkerId, Marker>{};
  Marker _marker;
  MarkerId _markerId;
  void preparemarker() {
    _markerId = MarkerId('12345');

    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(48, 48)),
            'assets/images/pickup_pin.png')
        .then((value) {
          print('done preparing');
      _marker = Marker(
        icon: value,
          markerId: _markerId,
          position: LatLng(-6.8126957, 39.2873169),
          infoWindow: InfoWindow(title: 'my current location'),
          onTap: () {
            print('location');
          });
           _markers[_markerId] = _marker;
           notifyListeners();
    }
    );

   
  }
   Map<MarkerId, Marker> get markers => _markers;
  

}
