import 'package:scoped_model/scoped_model.dart';
import 'package:udder/scoped_model/udder_connected_models.dart';
// main model= file to connect whole app to all scoped models(mixins)
class MainModel extends Model with 
UdderConnectedModel,
UtilityModel,
CarsModel,
GoogleMapModel{

}