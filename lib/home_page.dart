import 'package:flutter/material.dart';
import 'package:udder/components/choice_widget.dart';
import 'package:udder/components/home/input_location_card.dart';
import 'package:udder/map.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black45,
        leading: Icon(Icons.menu),
        title: Center(child: Text('UDDER')),
      ),
      body: Stack(children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.85,
          child: MapPage(),
        ),
        // hii ndo card ya kuita location
       InputLocationCard(),
      
       Align(alignment: Alignment.bottomCenter,
         child: ChoiceWidget()),
       

      ]),
    );
  }

  
}
