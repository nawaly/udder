import 'package:flutter/foundation.dart';

@immutable
class Car {
  final String time;
  final String numberOfPeople;
  final String price;

  Car(
      {@required this.time,
      @required this.numberOfPeople,
      @required this.price});
}

List<Car> cars = <Car>[
  Car(numberOfPeople: '6 PEOPLE', price: '\$4.00', time: '10 MIN'),
  Car(numberOfPeople: '4 PEOPLE', price: '\$3.66', time: '15 MIN'),
  Car(numberOfPeople: '3 PEOPLE', price: '\$34.44', time: '34 MIN'),
  Car(numberOfPeople: '6 PEOPLE', price: '\$7.33', time: '12 MIN'),
];
