import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:udder/scoped_model/main.dart';

class ChoiceWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return AnimatedContainer(
          duration: Duration(microseconds: 500),
          height: model.choiceWidgetHeight,
          child: Stack(
            children: <Widget>[
              model.choiceWidgetHeight == 300
                  ? Align(
                      alignment: Alignment.bottomCenter,
                      child: AnimatedOpacity(
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 40, left: 20, right: 20, bottom: 20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(25),
                                topRight: Radius.circular(25)),
                            color: Colors.grey,
                          ),
                          height: 275,
                          child: Container(
                            color: Colors.white,
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        padding: EdgeInsets.only(top: 10),
                                        child: RichText(
                                          textAlign: TextAlign.center,
                                          text: TextSpan(
                                              text: 'ETA\n',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 15),
                                              children: [
                                                TextSpan(
                                                    text: model
                                                        .availableCars[
                                                            model.currentIndex]
                                                        .time,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 20))
                                              ]),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        padding: EdgeInsets.only(top: 10),
                                        child: RichText(
                                          textAlign: TextAlign.center,
                                          text: TextSpan(
                                              text: 'MAX SIZE\n',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 15),
                                              children: [
                                                TextSpan(
                                                    text: model
                                                        .availableCars[
                                                            model.currentIndex]
                                                        .numberOfPeople,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 20))
                                              ]),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Divider(),
                                Padding(
                                  padding: const EdgeInsets.only(top: 15),
                                  child: RichText(
                                    text: TextSpan(
                                        text: 'MIN FAIR\n',
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 15),
                                        children: [
                                          TextSpan(
                                              text: model
                                                  .availableCars[
                                                      model.currentIndex]
                                                  .price,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 20))
                                        ]),
                                  ),
                                ),
                                Divider(),
                                FlatButton(
                                  child: Text('GET FARE ESTIMATE'),
                                  onPressed: () {},
                                )
                              ],
                            ),
                          ),
                        ),
                        duration: Duration(seconds: 2),
                        opacity: 1,
                      ),
                    )
                  : Container(),
              Container(
                padding: EdgeInsets.all(3),
                margin: EdgeInsets.only(left: 40, right: 40),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  color: Colors.white,
                ),
                height: 50,
                child: Row(
                  mainAxisAlignment:
                      MainAxisAlignment.spaceBetween, //space btn texts
                  children: <Widget>[
                    Container(
                      width: 80,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: model.currentIndex == 0
                              ? Colors.blue[200]
                              : Colors.white),
                      child: IconButton(
                          icon: Text('TAXI',
                              style: TextStyle(color: Colors.black)),
                          onPressed: () {
                            onTap(index: 0, model: model);
                          }),
                    ),
                    Container(
                      width: 80,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: model.currentIndex == 1
                              ? Colors.blue[200]
                              : Colors.white),
                      child: IconButton(
                        icon: Text('SuperX',
                            style: TextStyle(color: Colors.black)),
                        onPressed: () {
                          onTap(index: 1, model: model);
                        },
                      ),
                    ),
                    Container(
                      width: 80,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: model.currentIndex == 2
                              ? Colors.blue[200]
                              : Colors.white),
                      child: IconButton(
                        icon: Text('BLACK',
                            style: TextStyle(color: Colors.black)),
                        onPressed: () {
                          onTap(index: 2, model: model);
                        },
                      ),
                    ),
                    Container(
                      width: 80,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: model.currentIndex == 3
                              ? Colors.blue[200]
                              : Colors.white),
                      child: IconButton(
                        icon:
                            Text('SUV', style: TextStyle(color: Colors.black)),
                        onPressed: () {
                          onTap(index: 3, model: model);
                        },
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }

  void onTap({@required int index, @required MainModel model}) {
    // if not opened(50)=>(setChoice.) should open=>(setCurrent.) to open the index opened
    if (model.choiceWidgetHeight == 50) {
      model.setChoiceWidgetHeight(height: 300.0);
      model.setCurrentIndex(index: index);
    } else {
      // if press same tab again=> should close(50)
      if (model.currentIndex == index) {
        model.setChoiceWidgetHeight(height: 50.0);
      }
          //  index pressed should be it
      model.setCurrentIndex(index: index);
    }
  }
}
